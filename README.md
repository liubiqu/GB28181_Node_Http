# 基于NodeJS的GB 28181管理平台

--------------

## 一、功能介绍

- GB28181的设备注册到本平台，并发布为多种媒体格式(flv,rtmp,hls等)，支持浏览器播放视频
- 流媒体服务集成 [ZLMediaKit](https://github.com/xia-chu/ZLMediaKit)（Linux版本，感谢作者夏楚）
- 一台信令服务器可以支持多台媒体服务器
- 有丰富的restful API接口，方便与第三方程序集成

### 功能截图

#### 1. 播放界面

![播放界面](https://gitee.com/hfwudao/GB28181_Node_Http/raw/develop/%E7%95%8C%E9%9D%A2%E6%88%AA%E5%9B%BE/%E6%92%AD%E6%94%BE%E7%95%8C%E9%9D%A2.png)

#### 2. 设备列表界面

![设备列表](https://gitee.com/hfwudao/GB28181_Node_Http/raw/develop/%E7%95%8C%E9%9D%A2%E6%88%AA%E5%9B%BE/%E8%AE%BE%E5%A4%87%E5%88%97%E8%A1%A8.png)

## 二、技术框架

### 1. 前端

- [查看前端文档](https://gitee.com/hfwudao/GB28181_Node_Http/blob/master/CameraUI/README.md)

### 2. 后端

- 后端使用NodeJS开发，加入Express框架。
- apidoc 文档工具(安装脚本在setup.sh里)。
- test-api.http是VsCode的Rest Client插件的测试用例脚本。

使用时在VsCode的Workspace Settings-settings.json设置环境变量，如：

```json
{
    "rest-client.environmentVariables": {

        "$shared": {},
        "local":{
            "host":"localhost"
        },
        "test":{
            "host":"测试服务器ip"
        }
    }
}
```

### 3. 默认使用端口

- 管理界面 HTTP 端口: 7000
- zlmedia http: 9094
- zlmedia https: 9096
- zlmedia rtmp: 9092
- sip: udp 5060
- sip: tcp 5060
- 流媒体动态端口: tcp&udp 50000-50500
- 其它配置信息: data/constants.js

使用Ctrl+Alt+E来切换加载的配置项。

## 三、部署

### 1. 本项目集成了ZLMediaKit

ZLMediaKitCentOS 和 ZLMediaKitX64 分别放置了Linux和Windows 版本的运行程序。

### 2. 启动MediaServer

```bash
# CentOS 环境
cd ZLMediaKitCentOS
./MediaServer -d &
```

### 3. 需要准备好NodeJS环境

（步骤略）

### 4. 部署 GB28181_Node_Http 服务端

#### (1) 安装

```bash
# 安装
bash ./setup.sh
```

#### (2) 使用 node 启动程序

```bash
# 启动，程序自动启动ZLMediaKit ， 不需要专门部署
node gb28181_app.js
# 访问网址 http://你的ip:7000
```

#### (3) 如果使用nvm & pm2 ， 主要指令为

```bash
# 显示nvm 安装nodejs版本
nvm list
# 选择nodejs版本
nvm use 12.13.1
# 进入程序目录 
cd GB28181_Node_Http
# 查看pm2运行状态
pm2 list
# 停止进程
pm2 stop gb28181_app.js
# 启动进程
pm2 start gb28181_app.js
# 重启进程
pm2 restart gb28181_app.js
# 查看log
pm2 logs
# 在log中搜索
pm2 logs | grep 0000480
# 如果遇到日志过大，可以限制pm2日志数量
pm2 install pm2-logrotate
pm2 set pm2-logrotate:retain 50
```

### 5. 设置项

- data/cameras.db 是数据库，使用SQLite，初始化时自动生成
- data/constants.js 一些常量设置，一般不需要修改，但里面的ip要设置成服务器外网ip
- data/log.js 是log4js设置
- data/registry.data 是缓存文件，删除不影响使用
- 服务端的设置（如服务端设备账号等）在系统启动后，通过网页端设置

### 6. 设备端设置注意事项

- SIP服务器ID 34020000002000002250
- SIP服务器域 3402000000
- SIP服务器地址： 与摄像头设置的所在服务器对应
- SIP服务器端口 5060
- SIP用户认证ID
-- 摄像头   : 34020000001110000***,后面三位数对应平台的通道ID
-- NVR      : 34020000001320000***,后面三位数对应平台的通道ID
- 密码 12345678
- 本地SIP端口 5060
- 注册有效期 默认值
- 心跳周期 默认值
- 最大心跳超时次数 默认值
- NVR_音视频_选择通道 码流类型_主码流 视频类型_视频流，不能是复合流
- 视频通道编码ID 按顺序编写：
-- D1 3402000000131000001
-- D2 3402000000131000002
-- ...

## 四、更新日志

- 1.1.5 支持多个媒体服务器，取消自动启动媒体服务器
- 1.1.6 设备加搜索
- 1.1.7 有的设备把ParentID值赋值为SIP Server的值，统一改为父域值
- 1.1.8 处理定时报警图片
- 1.1.9 修改密码功能
- 1.1.11 代码逻辑优化，setup优化，sip生成指令的类拆分

### TODO List

与 node_modules/sip/sip.js库存在不兼容情况，需要修改304行代码：

```js

// 其中 via.host的值是主机名
if (via.host && via.host == 'VM_16_10_centos') {
    return '';
}

```

- digest.js 改了 148行

```js
  //TODO: 这里不知道干什么用，会造成鉴权失败
  if(!ctx.nc){ // 加if的
    ctx.nc = (ctx.nc || 0) +1;
  }
```

- 播放NVR录像
- 电视墙
- 云台控制
- 权限控制

## 五、问题处理

### 1. flash无法播放视频（或https---flv无法播放）

看服务器是否缺少libssl组件，一般在ZLMediaKit的log会有提示
在CentOS7下安装libssl过程：

```bash
wget https://www.openssl.org/source/old/1.0.1/openssl-1.0.1e.tar.gz
tar -zxvf openssl-1.0.1e.tar.gz
cd openssl-1.0.1e/
./config shared zlib-dynamic
make
ll
cp libssl.so.1.0.0 /usr/lib64/
cp libcrypto.so.1.0.0 /usr/lib64/
cd /usr/lib64
ln -s libssl.so.1.0.0 libssl.so.10
ln -s libcrypto.so.1.0.0 libcrypto.so.10
```

### 2. Windows环境，如果提示缺少 vcruntime140_1

把ZLMediaKitX64文件夹下的vcruntime140_1.dll 拷贝到:
操作系统的 Windows/System32 目录下。
