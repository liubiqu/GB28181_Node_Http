import request from '@/utils/request'

/**
 * 获取通道列表
 * @param {Object} data
 */
export function list(data) {
  return request({
    url: '/v1/channel/list',
    method: 'get',
    params: data
  })
}

export function deleteByChannelDevice(data) {
  return request({
    url: '/v1/channel/deleteByChannelDevice', method: 'POST', data
  })
}

export function move(data) {
  return request({
    url: '/v1/channel/move',
    method: 'get',
    params: data
  })
}

export function zoom(data) {
  return request({
    url: '/v1/channel/zoom',
    method: 'get',
    params: data
  })
}
