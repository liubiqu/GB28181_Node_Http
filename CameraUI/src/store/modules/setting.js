import { save } from '@/api/setting'

const getDefaultState = () => {
  return {

  }
}
const state = getDefaultState()

const mutations = {

}
const actions = {
  save({ commit }, data) {
    console.log('save data', commit, data)
    return new Promise((resolve, reject) => {
      save(data).then(() => {
        resolve()
      }).catch(error => {
        console.error(error)
        reject(error)
      })
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
