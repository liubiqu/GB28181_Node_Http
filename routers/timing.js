const express = require('express');
const router = express.Router();
const constants = require('../data/constants')

const timingController = require('../controller/v1/timing');

/**
 * @api {get} /timing/list 定时截图列表
 * @apiDescription 定时截图列表
 * @apiName TimingList
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'timing/list', timingController.list);


module.exports = router;