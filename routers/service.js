const express = require('express');
const router = express.Router();
const constants = require('../data/constants')

const serviceController = require('../controller/v1/service');

/**
 * @api {post} /service/changeStatus GB28181服务启停
 * @apiDescription Service服务启停
 * @apiName ServiceChangeStatus
 * @apiGroup Service接口
 * @apiVersion 1.0.0
 */
router.post(constants.version +'service/changeStatus',serviceController.changeStatus);

/**
 * @api {post} /service/getStatus 获取GB28181服务状态
 * @apiDescription 获取Service服务状态
 * @apiName ServiceGetStatus
 * @apiGroup Service接口
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'service/getStatus', serviceController.getStatus);

router.get(constants.version + 'service/getClients',serviceController.getClients);

module.exports = router;