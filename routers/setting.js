const express = require('express');
const router = express.Router();
const constants = require('../data/constants');
const settingController = require('../controller/v1/setting');

/**
 * @api {get} /setting/load 加载设置
 * @apiDescription 加载设置
 * @apiName SettingLoad
 * @apiGroup 设置项管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'setting/load', settingController.load);

/**
 * @api {post} /setting/save 保存设置
 * @apiDescription 保存设置
 * @apiName SettingSave
 * @apiGroup 设置项管理
 * @apiParam {string} sip_command_account 服务器sip帐号
 * @apiParam {string} sip_command_password 服务器sip密码
 * @apiParam {string} server_realm 服务器sip域
 * @apiParam {string} sip_command_host 服务器sip ip地址
 * @apiParam {int} keep_alive 心跳时间
 * @apiVersion 1.0.0
 */
router.post(constants.version+ 'setting/save', settingController.save);

module.exports = router;