const express = require('express');
const router = express.Router();
const constants = require('../data/constants')

const mediaController = require('../controller/v1/media');

router.get('/', mediaController.index);

/**
 * HOOK，给ZL回调使用
 */
router.get(constants.version + 'media/getServerConfig',        mediaController.getServerConfig);
router.get(constants.version + 'media/rtp_list',               mediaController.rtpList);
router.post(constants.version + 'media/on_stream_not_found',   mediaController.onStreamNotFound);
router.post(constants.version + 'media/on_server_started',     mediaController.onServerStarted);
router.post(constants.version + 'media/on_stream_none_reader', mediaController.onStreamNoneReader);
router.post(constants.version + 'media/on_play',               mediaController.onPlay);
router.post(constants.version + 'media/on_stream_changed',     mediaController.onStreamChanged);
router.post(constants.version + 'media/on_publish',            mediaController.onPublish);
router.post(constants.version + 'media/on_http_access',        mediaController.onHttpAccess);
router.post(constants.version + 'media/on_flow_report',        mediaController.onFlowReport);
module.exports = router;