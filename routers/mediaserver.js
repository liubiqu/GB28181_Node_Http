const express = require('express');
const router = express.Router();
const constants = require('../data/constants')

const mediaServerController = require('../controller/v1/mediaserver');

/**
 * @api {get} /mediaserver/list 查询媒体服务器列表
 * @apiDescription 查询媒体服务器列表
 * @apiName MediaServerList
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'mediaserver/list', mediaServerController.list);

/**
 * @api {get} /mediaserver/query 查询
 * @apiDescription 查询一个媒体服务器信息
 * @apiName MediaServerQuery
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.get(constants.version + 'mediaserver/query', mediaServerController.query);

/**
 * @api {post} /mediaserver/del 删除
 * @apiDescription 删除一个媒体服务器信息
 * @apiName MediaServerDel
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.post(constants.version + 'mediaserver/del', mediaServerController.del);

/**
 * @api {post} /mediaserver/insert 新的媒体服务器信息
 * @apiDescription 插入一个媒体服务器信息
 * @apiName MediaServerInsert
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.post(constants.version + 'mediaserver/insert', mediaServerController.insert);

/**
 * @api {post} /mediaserver/update 更新媒体服务器信息
 * @apiDescription 更新一个媒体服务器信息
 * @apiName MediaServerUpdate
 * @apiGroup 设备管理
 * @apiVersion 1.0.0
 */
router.post(constants.version + 'mediaserver/update', mediaServerController.update);
module.exports = router;