echo "1. 编译前端程序并拷贝到 public/ 目录"
cd CameraUI
npm install -g vue-cli --registry=https://registry.npm.taobao.org
npm i --registry=https://registry.npm.taobao.org
npm run build:prod
echo "2. 安装后端程序依赖"
cd ..
npm i --registry=https://registry.npm.taobao.org
echo "3. 安装 apidoc 工具"
npm install apidoc@0.17 -g --registry=https://registry.npm.taobao.org
echo "4. 生成api文档并拷贝到 public/ 目录"
apidoc -i routers/ -o public/apidoc/
echo "5. 设置可执行文件权限"
chmod +x ZLMediaKitCentOS/MediaServer

echo "安装"

wget http://ftp.icm.edu.pl/pub/unix/graphics/GraphicsMagick/1.3/GraphicsMagick-1.3.21.tar.gz
tar zxvf GraphicsMagick-1.3.21.tar.gz
rm -f GraphicsMagick-1.3.21.tar.gz
cd GraphicsMagick-1.3.21
./configure
make -j8
make install
gm version

echo " *******************************************"
echo " * 安装完成，请将 constants.demo.js 改名为constants.js 并配置环境变量"
echo " * 将服务器外网ip 写在 constants.js 的ip值上"
echo " * 然后使用 node gb28181_app.js 命令启动程序"
echo " * 或者使用 pm2 start gb28181_app.js 作为服务启动程序"
