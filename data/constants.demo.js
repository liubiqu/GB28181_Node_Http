const path = require('path');
/**
 * 常量设置
 */
module.exports = {
    // api版本
    'version':'/api/v1/',
    /**
     * http接口状态码
     */
    'httpCode':{
        'OK'            : {code: 20000, message: '操作成功'},
        'ILLEGAL_TOKEN' : {code: 50008, message: '无效凭据'},
        'OTHER_LOGIN'   : {code: 50012, message: '已登陆'},
        'TOKEN_EXPIRE'  : {code: 50014, message: '登陆过期'},
        'PASS_ERROR'    : {code: 60204, message: '密码错误'},
        'ILLEGAL_PARAM' : {code: 60205, message: '无效参数'},
        'NO_DATA'       : {code: 60206, message: '未找到数据'},
        'DUPLITED'      : {code: 60207, message: '数据已经存在'},
        'UNKNOWN' : {code: 60205, message: '未知错误'}
    },
    'http': {
        'port'          : 7000
    },
    // sqlite 数据库位置
    'dbPath':__dirname + '/cameras.db',
    // 报警图片文件夹，程序监控此文件的变化
    'alertBasePath':__dirname.split(path.sep).slice(0, __dirname.split(path.sep).length-1).join('/') + '/public/static/img/capture/',
    // 报警文件的文件格式
    'fileRegex':{
        'hikvision':{
            regex: /[\d|.]+(_01)?_(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{3})_([A-Z_]*).jpg/,
            regex_name: ['file', null, 'year', 'month', 'day', 'hour', 'minute', 'second', 'ms', 'type']
        }
    },
    // 摄像机上传的图片分类存放到下面地址
    'cameraBak':{
        'savePath':{
            'AI_OPEN_PLAT':__dirname + '/public/static/img/alerts/',
            'TIMING':__dirname + '/public/static/img/timing/'
        },
        'url':{
            'AI_OPEN_PLAT':'/static/img/alerts/',
            'TIMING':'/static/img/timing/'
        }
    },
    // 几秒内重复报警忽略
    'alertInterval':40,
    // 初始化的默认用户名密码
    'defaultAdmin':{
        'username': 'admin',
        'password': '123456'
    },
    // sip server信息
    'sip': {
        'version': '2.0',
        'udp': true,
        'tcp': true,
        'useragent': 'NODE GB28181 SERVER V1',
        // 是否鉴权
        'authorize': true
    },

    'logger': {
        'filecount': 5,   // 日志文件保存数量
        'path': 'logs/'  // GB日志默认存储位置，注意ZLMediaKit的日志位置不是在这里设置的
    },
    registry: {
        invites: {}
    },
    // 媒体服务器信息
    mediaservers: {},
    ip: '127.0.0.1',
    // 程序运行开始时间
    start: new Date(),
    rstring: function () {
        return Math.floor(Math.random() * 1e6).toString();
    }
}
