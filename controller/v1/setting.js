const constants = require('../../data/constants');
const settingModel = require('../../model/setting');
const sipServer = require('../../sip/server');
const basectrl    = require('./basectrl');
/**
 * 系统设置项
 */
let Setting={
    
    /**
     * 加载设置
     * @param {*} req
     * @param {*} res
     */
    load: async function (req, res) {
        if (!basectrl.hasLogin(req, res)) {
            return;
        }
        const row = await settingModel.load();
        let data = Object.assign(constants.httpCode.OK, {data: row});
        res.send(data);
    },
    save: async function (req, res) {
        if (!basectrl.hasLogin(req, res)) {
            return;
        }
        const row = settingModel.save(req.body);
        let data = Object.assign(constants.httpCode.OK, {data: row});
        res.send(data);
        // 重启sipServer
        const sipSetting = settingModel.load();
        if (sipSetting.sip_rtp_host) {
            // 启动gb28181
            sipServer.restart(sipSetting);
        }
    }
};

module.exports=Setting;