const sip = require("sip");
const constants = require("../../data/constants");
const digest = require("sip/digest");

/**
 * 回应心跳信令，如果设备未注册发送 Unauthorized
 * @param {*} rq
 */
module.exports = {

    generate(rq) {
        const rs = sip.makeResponse(rq, 200, 'OK');
        const deviceId = sip.parseUri(rq.headers.from.uri).user;
        if (!constants.registry[deviceId]) {
            let userinfo = {username: deviceId};
            userinfo.session = userinfo.session || {realm: constants.sip.option.server_realm};
            let ret = digest.challenge(userinfo.session, sip.makeResponse(rq, 401, 'Unauthorized'));
            sip.send(ret);
        } else {
            constants.registry[deviceId].online = true;
            constants.registry[deviceId].last = (new Date()).getTime();
        }
        return rs;
    }
}
