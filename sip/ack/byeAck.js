const sip = require("sip");

/**
 * 生成 bye 响应信令
 */
module.exports={
    generate(rq){
        return sip.makeResponse(rq, 200, 'OK');
    }
}
