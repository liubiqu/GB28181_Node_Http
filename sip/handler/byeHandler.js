const byeAck = require("../ack/byeAck");
const sip = require("sip");
module.exports = {
    handle: (rq) => {
        let byeAckMsg = byeAck.generate(rq);
        sip.send(byeAckMsg);
    }
}
