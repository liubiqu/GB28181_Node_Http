const channelModel = require("../../../model/channel");
const stringUtils = require('../../../utils/stringUtils');
const log4js = require('../../../data/log');
const sip = require("sip");
const logger = log4js.getLogger('sip');
const catalogFactory = require('../../factory/catalogFactory');

module.exports= {
    async handle(deviceInfo, response) {
        // 暂未使用
        logger.info('deviceInfo response', response);

        let DeviceType = response.DeviceType.toString();
        if (DeviceType === 'IPC') {
            deviceInfo.sumnum = response.MaxCamera.toString();
            let temp1 = Object.assign({
                Name: [''],
                Owner: [''],
                ParentID: [response.DeviceID.toString()],
                CivilCode: [''],
                Address: [''],
                RegisterWay: ['1'],
                Secrecy: ['0']
            }, response);

            let channelId = temp1.DeviceID.toString();
            if (!deviceInfo.channels[channelId]) {
                // 子通道赋初值
                deviceInfo.channels[channelId] = Object.assign({}, deviceInfo);
                delete deviceInfo.channels[channelId]['channels'];
                delete deviceInfo.channels[channelId]['sip_command_host'];
                delete deviceInfo.channels[channelId]['sip_command_port'];

                deviceInfo.channels[channelId].deviceid = temp1.DeviceID.toString();
                if (!deviceInfo.channels[channelId].uri) {
                    logger.warn('这里子通道没有定义 看看处理逻辑有没有问题', deviceInfo.channels[channelId]);
                } else {
                    deviceInfo.channels[channelId].uri = deviceInfo.channels[channelId].uri.replace(deviceInfo.deviceid, channelId);
                }
                deviceInfo.channels[channelId].parentid = deviceInfo.deviceid;
            }

            await channelModel.updateDeviceInfo(temp1);
        } else {
            // NVR ，发目录查询指令
            let catalogCmd = catalogFactory.generate(deviceInfo);
            if (catalogCmd) {
                sip.send(catalogCmd);
            }
        }
    }
};