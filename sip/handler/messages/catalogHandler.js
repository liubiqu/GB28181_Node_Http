const channelModel = require("../../../model/channel");
const stringUtils = require('../../../utils/stringUtils');
const deviceModel = require('../../../model/device');
const log4js = require('../../../data/log');
const logger = log4js.getLogger('sip');

module.exports={
    handle(deviceInfo, response){
        // 通道数量
        deviceInfo.sumnum =parseInt(response.SumNum.toString());

        response.DeviceList[0].Item.map(async (temp)=>{
            // 有的设备把ParentID值赋值为SIP Server的值，这里统一改为设备deviceId值
            temp.ParentID = deviceInfo.deviceid;
            const channelId=temp.DeviceID.toString();
            const subChannelName = temp.Name.toString();
            if (subChannelName) {
                //转为utf8
                temp.Name =stringUtils.gb2312ToUtf8(subChannelName);
            }

            if (!deviceInfo.channels[channelId]) {
                let subChannelInfo = deviceModel.generateEmptyChannel(deviceInfo, channelId);
                if (!subChannelInfo.uri) {
                    logger.warn('uri没定义，检查逻辑', deviceInfo.channels[channelId]);
                } else {
                    subChannelInfo.uri = subChannelInfo.uri.replace(deviceInfo.deviceid, channelId);
                }
                subChannelInfo.parentid = deviceInfo.deviceid;
                deviceInfo.channels[channelId] = subChannelInfo;
            }

            logger.info("[目录更新] temp=", temp);
            await channelModel.updateDeviceInfo(temp);
        });
    }
}
