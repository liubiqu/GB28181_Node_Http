const inviteAck = require("../ack/inviteAck");
const sip = require("sip");

module.exports={
    handle(rq){
        // 回复ack
        let inviteAckMsg = inviteAck.generate(rq);
        sip.send(inviteAckMsg);
    }
};
