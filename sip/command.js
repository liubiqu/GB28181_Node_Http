/**
 * GB28181 对sip命令的封装
 */
const digest = require('sip/digest');
const constants = require('../data/constants');
const sip = require('sip');
/**
 * 封装发送的命令
 */
let command = {
    /**
     * 要求客户端重新登陆
     * @param {*} rq
     */
    registerFailAck: function (rq) {
        let session = {realm: constants.sip.option.server_realm};
        return digest.challenge(session, sip.makeResponse(rq, 401, 'Unauthorized'));
    },
    //TODO: 云台移动
    move: function () {

    },
    //TODO: 放大缩小
    zoom: function () {

    }
};

module.exports = command;
