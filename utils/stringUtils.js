const iconv = require("iconv-lite");

module.exports={
    gb2312ToUtf8(input){
        let bufs = iconv.decode(input, 'gb2312');
        return [bufs.toString('utf8')];
    }
}