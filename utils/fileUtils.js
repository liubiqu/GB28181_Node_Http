const path = require('path');
const fs   = require('fs');

let fileUtils={
    makeDir(dirpath) {
        if (!fs.existsSync(dirpath)) {
            let pathtmp;
            dirpath.split('/').forEach(function(dirname) {
                if (pathtmp) {
                    pathtmp = path.join(pathtmp, dirname);
                }
                else {   //如果在linux系统中，第一个dirname的值为空，所以赋值为"/"
                    if(dirname){
                        pathtmp = dirname;
                    }else{
                        pathtmp = "/";
                    }
                }
                if (!fs.existsSync(pathtmp)) {
                    fs.mkdirSync(pathtmp);
                }
            });
        }
        return true;
    }
};

module.exports = fileUtils;
