let db = require('../db/dbmanager');
const constants = require('../data/constants');
const log4js = require('../data/log');
const logger = log4js.getLogger('sip');

/**
 * 服务器信息
 */
let MediaServer={
    cache:null,
    /**
     * 插入媒体服务器
     */
    insert: function(data){
        return new Promise(function (resolve) {
            let param = {
                $server_id: data.server_id,
                $media_host: data.media_host,
                $intranet: data.intranet,
                $media_ip: data.media_ip,
                $http_port: data.http_port,
                $https_port: data.https_port,
                $rtmp_port: data.rtmp_port,
                $rtp_min_port: data.rtp_min_port,
                $rtp_max_port: data.rtp_max_port,
                $secret: data.secret
            };
            const sql = "INSERT INTO MEDIASERVER(server_id,media_host,media_ip,intranet,http_port,https_port,rtmp_port,rtp_min_port,rtp_max_port,secret)VALUES($server_id,$media_host,$media_ip,$intranet,$http_port,$https_port,$rtmp_port,$rtp_min_port,$rtp_max_port,$secret)";

            db.sqliteDB.run(sql, param);
            MediaServer.cache = null;
            db.sqliteDB.lastRow("DEVICE", (d) => {
                resolve(d);
            });
        });
    },
    /**
     * 更新媒体服务器信息
     */
    update: function(newdata, callback){
        let sqldata = {$server_id: newdata.server_id};
        let newsql = '';
        if(newdata.media_host){
            sqldata.$media_host = newdata.media_host;
            if(newsql) newsql+=',';
            newsql += "media_host=$media_host";
        }
        if(newdata.media_ip){
            sqldata.$media_ip = newdata.media_ip;
            if(newsql) newsql+=',';
            newsql += "media_ip=$media_ip";
        }
        if(newdata.intranet){
            sqldata.$intranet = newdata.intranet;
            if(newsql) newsql+=',';
            newsql += "intranet=$intranet";
        }
        if(newdata.http_port){
            sqldata.$http_port = newdata.http_port;
            if(newsql) newsql+=',';
            newsql += "http_port=$http_port";
        }
        if(newdata.https_port){
            sqldata.$https_port = newdata.https_port;
            if(newsql) newsql+=',';
            newsql += "https_port=$https_port";
        }
        if(newdata.rtmp_port){
            sqldata.$rtmp_port = newdata.rtmp_port;
            if(newsql) newsql+=',';
            newsql += "rtmp_port=$rtmp_port";
        }
        if(newdata.rtp_min_port){
            sqldata.$rtp_min_port = newdata.rtp_min_port;
            if(newsql) newsql+=',';
            newsql += "rtp_min_port=$rtp_min_port";
        }
        if(newdata.rtp_max_port){
            sqldata.$rtp_max_port = newdata.rtp_max_port;
            if(newsql) newsql+=',';
            newsql += "rtp_max_port=$rtp_max_port";
        }
        if(newdata.secret){
            sqldata.$secret = newdata.secret;
            if(newsql) newsql+=',';
            newsql += "secret=$secret";
        }
        const sql = "update MEDIASERVER set " + newsql + " where server_id=$server_id";
        logger.info(sql, sqldata);
        db.sqliteDB.run(sql,sqldata);
        MediaServer.cache=null;
        if(callback){
            callback(newdata);
        }
    },
    del:function(server_id){
        let param={
            $server_id: server_id
        };
        const sql = "DELETE FROM MEDIASERVER WHERE server_id=$server_id";
        db.sqliteDB.run(sql,param);
        MediaServer.cache = null;
    },
    getById:async function(id){
        if(!MediaServer.cache){
            await MediaServer.reloadCache();
        }
        return new Promise(function (resolve) {
            let item = MediaServer.cache.find((value) => {
                return value.id === id
            });
            resolve(item)
        });
    },
    getByServerId: function(server_id){
        return new Promise(async function(resolve){
            if(!MediaServer.cache || !MediaServer.cache.secret){
                await MediaServer.reloadCache();
            }
            let item = MediaServer.cache.find((value)=>{
                return value.server_id === server_id
            });
            if(!item){
                await MediaServer.reloadCache();
                item = MediaServer.cache.find((value)=>{
                    return value.server_id === server_id
                });
            }
            resolve(item);
        });
    },
    reloadCache: function(){
        return new Promise(function (resolve) {
            if (!MediaServer.cache|| !MediaServer.cache.secret) {
                const sql = "SELECT * from MEDIASERVER order by id DESC";
                db.sqliteDB.queryData(sql, function (rows) {
                    MediaServer.cache = Object.assign(rows);
                    resolve(MediaServer.cache);
                });
            } else {
                resolve(MediaServer.cache);
            }
        });
    },
    /**
     * 查询列表
     * @param {*} page 第几页
     * @param {*} size 每页大小
     */
    list:function(page,size){
        return new Promise(function (resolve) {
            if (MediaServer.cache) {
                resolve(MediaServer.cache);
            } else {
                const sql = `SELECT * from MEDIASERVER order by id DESC limit ${size} offset ${size}*${page}`;
                console.info(sql);
                db.sqliteDB.queryData(sql, function (rows) {
                    MediaServer.cache = Object.assign(rows);
                    resolve(MediaServer.cache);
                });
            }
        });
    },
    count:function(){
        return new Promise(function (resolve) {
            let sql = "SELECT COUNT(*) as c FROM MEDIASERVER";
            db.sqliteDB.oneData(sql, (ret) => {
                resolve(ret.c);
            });
        });
    },
    getInfoById: async function(id){
        let publicIp = constants.ip;
        return new Promise(async function (resolve, reject) {
            let item = await MediaServer.getById(id);
            // logger.debug('媒体服务器信息:', item);
            if (item) {
                item.local_host = item.media_ip;

                // 如果在同一台电脑，可能会出现访问公网IP无法连接到端口
                // 这里强制连接本机ip
                // if (publicIp === item.media_ip) {
                //     item.local_host = '127.0.0.1';
                // }
                resolve(Object.assign({
                    // 这个地址用来请求ZLMediaKit的API使用
                    zlk_api_host: `http://${item.local_host}:${item.http_port}/`,
                    url: `http://${item.media_host}:${item.http_port}/`,
                    urlTls: `https://${item.media_host}:${item.https_port}/`,
                    rtmp: `rtmp://${item.media_host}:${item.rtmp_port}/`,
                    rtmp_snap: `rtmp://${item.local_host}:${item.rtmp_port}/`
                },item));
            } else {
                reject();
            }
        });
    },
    getInfoByServerId :async function(serverId){
        let publicIp = constants.ip;
        return new Promise(function (resolve, reject) {
            MediaServer.getByServerId(serverId).then((item) => {
                logger.info('从数据库或缓存读 getInfoByServerId,返回 item', item, 'publicIp=', publicIp, 'serverId=', serverId);
                if (item) {
                    item.local_host = item.intranet;

                    // 如果在同一台电脑,可能会出现访问公网IP无法连接到端口
                    // 这里强制连接本机ip
                    // if (publicIp === item.media_ip) {
                    //     item.local_host = '127.0.0.1';
                    // }
                    resolve(Object.assign({
                        // 这个地址用来请求ZLMediaKit的API使用
                        zlk_api_host: `http://${item.local_host}:${item.http_port}/`,
                        url: `http://${item.media_host}:${item.http_port}/`,
                        urlTls: `https://${item.media_host}:${item.https_port}/`,
                        rtmp: `rtmp://${item.media_host}:${item.rtmp_port}/`,
                        rtmp_snap: `rtmp://${item.local_host}:${item.rtmp_port}/`
                    },item));
                } else {
                    reject(`no media server,serverId=${serverId}`);
                }
            })
        });
    }
};

module.exports=MediaServer;
