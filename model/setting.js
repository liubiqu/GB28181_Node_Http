let db = require('../db/dbmanager');

/**
 * 设置信息
 */
let Setting={
    cache:null,
    /**
     * @param {*} newData 保存数据
     */
    save:function(newData){
        let me = this;
        me.cache = newData;

        return new Promise(async function (resolve) {
            const dbData = await me.load();
            if (dbData) {
                await me.update(newData);
            } else {
                await me.insert(newData);
            }
            resolve(newData);
        });
    },
    /**
     * 插入数据
     */
    insert:function(newData){
        let sqlData={
            $sip_command_account  : newData.sip_command_account,
            $sip_command_password : newData.sip_command_password,
            $sip_command_host     : newData.sip_command_host,
            $server_realm         : newData.server_realm,
            $sip_command_port     : newData.sip_command_port,
            $keep_alive_second    : newData.keep_alive_second,
            $sip_rtp_port         : newData.sip_rtp_port,
            $server_hls_port      : newData.server_hls_port,
            $snap_path            : newData.snap_path
        };
        return new Promise(function (resolve) {
            const sql = "insert into SETTING(sip_command_account,sip_command_password,sip_command_host,server_realm,sip_command_port,keep_alive_second,sip_rtp_port,server_hls_port,snap_path"
                +
                ") values(" +
                "$sip_command_account,$sip_command_password,$sip_command_host,$server_realm,$sip_command_port,$keep_alive_second,$sip_rtp_port,$server_hls_port,$snap_path)";
            db.sqliteDB.run(sql, sqlData);
            resolve(newData);
        });
    },
    /**
     * 更新
     */
    update:function(newData){
        let me=this;
        let sqlData={
            $sip_command_account  : newData.sip_command_account,
            $sip_command_password : newData.sip_command_password,
            $sip_command_host     : newData.sip_command_host,
            $sip_command_port     : newData.sip_command_port,
            $server_realm         : newData.server_realm,
            $sip_rtp_host         : newData.sip_rtp_host,
            $sip_rtp_port         : newData.sip_rtp_port,
            $server_hls_port      : newData.server_hls_port,
            $keep_alive_second    : newData.keep_alive_second,
            $snap_path            : newData.snap_path
        };
        return new Promise(function (resolve) {
            const sql = "update SETTING set " +
                "sip_command_account = $sip_command_account,sip_command_password= $sip_command_password," +
                "sip_command_host=$sip_command_host,server_realm=$server_realm,sip_command_port=$sip_command_port,keep_alive_second=$keep_alive_second," +
                "sip_rtp_port=$sip_rtp_port," +
                "sip_rtp_host=$sip_rtp_host," +
                "snap_path=$snap_path," +
                "server_hls_port=$server_hls_port" +
                " where id=1";
            db.sqliteDB.run(sql, sqlData);
            me.cache = newData
            resolve(newData);
        });
    },
    /**
     * 加载设置
     */
    load:function(){
        let me=this;
        return new Promise(function (resolve) {
            const sql = "SELECT * FROM SETTING WHERE id=1";
            db.sqliteDB.oneData(sql, async (data) => {
                if (!data || data.length === 0) {
                    let newData = {
                        sip_command_account: '34010300002000002250',
                        sip_command_password: '12345678',
                        sip_command_host: 'localhost',
                        sip_command_port: 5060,
                        server_realm: '3401030000',
                        keep_alive_second: 60,
                        snap_path: '',
                        sip_rtp_port: 9092,
                        sip_rtp_host: 'localhost',
                        server_hls_port: 9094
                    };
                    resolve(await me.insert(newData));
                } else {
                    me.cache = data;
                    resolve(data);
                }
            });
        });
    }
};

module.exports=Setting;
